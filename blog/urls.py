from django.urls import path
from . import views
from .views import (
    PostListView,
    PostDetailView,
    PostCreateView,
    PostUpdateView,
    PostDeleteView,
    UserPostListView,
    PostLikeToggle,
    PostLikeAPIToggle,
)

urlpatterns = [
    path('', PostListView.as_view(), name='blog-home'),
    path('post/<slug:slug>/', PostDetailView.as_view(), name='post-detail'),
    path('post/<slug:slug>/like/', PostLikeToggle.as_view(), name='like-toggle'),
    path('api/<slug:slug>/like/',
         PostLikeAPIToggle.as_view(), name='like-api-toggle'),
    path('create/new/', PostCreateView.as_view(), name='post-create'),
    path('post/<slug:slug>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<slug:slug>/delete/', PostDeleteView.as_view(), name='post-delete'),
    # path('users/', ),
    path('users/<str:username>', UserPostListView.as_view(), name='user-posts'),
    path('post/<slug:slug>/comment/', views.comment, name='comments'),
    path('tag/<slug:slug>/', views.tag_detail, name='tag-detail'),
]
